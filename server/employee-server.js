const express = require('express')
const bodyParser = require('body-parser')


const cors = require('cors')





// routers
const employeeRouter = require('./employee/routes/employee')


const app = express()
app.use(cors('*'))

app.use(bodyParser.json())






// add a middleware for getting the id from token
// function getUserId(request, response, next) {

//   if (request.url == '/admin/signin' 
//       || request.url == '/admin/signup'
//        ) {
//     // do not check for token 
//     next()
//   } else {

//     try {
//       const token = request.headers['token']
//       const data = jwt.verify(token, config.secret)

//       // add a new key named userId with logged in user's id
//       request.userId = data['id']

//       // go to the actual route
//       next()
      
//     } catch (ex) {
//       response.status(401)
//       response.send({status: 'error', error: 'protected api'})
//     }
//   }
// }

//app.use(getUserId)


// add the routes
app.use('/employee', employeeRouter)


// default route
app.get('/', (request, response) => {
  response.send('welcome employee to my application')
})

app.listen(3001, '0.0.0.0', () => {
  console.log('server started on port 3001')
})