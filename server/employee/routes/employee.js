const express = require('express')
const utils = require('../../utils')
const db = require('../../db')

const crypto = require('crypto-js')


const router = express.Router()








// ----------------------------------------------------
// GET
// ----------------------------------------------------

// - viewLeavesStatus()
router.get('/viewLeavesStatus', (request, response) => {
    const statement = `
    select  * from apply_leave
  
    `
    db.query(statement, (error, datas) => {
      if (error) {
        response.send({status: 'error', error: error})
      } else {
        if (datas.length == 0) {
          response.send({status: 'error', error: 'data does not exist'})
        } else {
         
          response.send(utils.createResult(error, datas))
        }
      }
    })
  })
  
  // ----------------------------------------------------

//  meetingDetailsList

router.get('/meetingDetailsList', (request, response) => {
    const statement = `
    select  * from meeting
  
    `
    db.query(statement, (error, datas) => {
      if (error) {
        response.send({status: 'error', error: error})
      } else {
        if (datas.length == 0) {
          response.send({status: 'error', error: 'data does not exist'})
        } else {
         
          response.send(utils.createResult(error, datas))
        }
      }
    })
  })

// ----------------------------------------------------


  //viewSalarySlip


router.get('/viewSalarySlip', (request, response) => {
    const statement = `
    select  * from salary_slip
  
    `
    db.query(statement, (error, datas) => {
      if (error) {
        response.send({status: 'error', error: error})
      } else {
        if (datas.length == 0) {
          response.send({status: 'error', error: 'data does not exist'})
        } else {
         
          response.send(utils.createResult(error, datas))
        }
      }
    })
  })

// ----------------------------------------------------


// - knowappliedQueryStatus()

router.get('/QueryStatus', (request, response) => {
    const statement = `
    select  * from emp_queries
  
    `
    db.query(statement, (error, datas) => {
      if (error) {
        response.send({status: 'error', error: error})
      } else {
        if (datas.length == 0) {
          response.send({status: 'error', error: 'data does not exist'})
        } else {
         
          response.send(utils.createResult(error, datas))
        }
      }
    })
  })

// ----------------------------------------------------
// POST
// ----------------------------------------------------

// ----------------------------------------------------


router.post('/signin', (request, response) => {
  const {email, password} = request.body
  const statement = `select * from employee where email = '${email}' and password = '${crypto.SHA256(password)}'`
  db.query(statement, (error, employees) => {
    if (error) {
      response.send({status: 'error', error: error})
    } else {
      if (employees.length == 0) {
        response.send({status: 'error', error: 'employee does not exist'})
      } else {
        const employee = employees[0]
       
        response.send(utils.createResult(error, {
          FirstName: employee['FirstName'],
          LastName: employee['LastName']
          
        }))
      }
    }
  })
})




// ----------------------------------------------------

//applyLeave()

router.post('/applyLeave', (request, response) => {
    const {   leaveDate,  leavePurpose,   empId} = request.body
  
    
    const statement = `insert into apply_leave (   leaveDate,  leavePurpose, empId ) values (
       '${leaveDate}', '${leavePurpose}', '${empId}' 
    )`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })
  



// ----------------------------------------------------

//addNewQueries

router.post('/addNewQueries', (request, response) => {
    const {   queryTitle,  queryDesc,   empId} = request.body
  
    
    const statement = `insert into emp_queries ( queryTitle, queryDesc, empId ) values (
      '${queryTitle}', '${queryDesc}', '${empId}' 
    )`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })

// ----------------------------------------------------
// PUT
// ----------------------------------------------------

//editProfile
router.put('/editProfile/:empId', (request, response) => {
  const {empId} = request.params
  const {empName, address , mobile ,birth_date} = request.body
  const statement = `update employee set empName = '${empName}' , address = '${address}',mobile = '${mobile}' ,birth_date = '${birth_date}' 
  where empId = ${empId}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})



// ----------------------------------------------------

module.exports = router