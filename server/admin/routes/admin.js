const express = require('express')
const utils = require('../../utils')
const db = require('../../db')

const crypto = require('crypto-js')


const router = express.Router()





// ----------------------------------------------------
// GET
// ----------------------------------------------------

//meetingListAndMeetingStatus()
router.get('/meetingListAndMeetingStatus', (request, response) => {
  const statement = `
  select  * from meeting

  `
  db.query(statement, (error, datas) => {
    if (error) {
      response.send({status: 'error', error: error})
    } else {
      if (datas.length == 0) {
        response.send({status: 'error', error: 'data does not exist'})
      } else {
       
        response.send(utils.createResult(error, datas))
      }
    }
  })
})


// ----------------------------------------------------


//viewEmpAppliedLeave
router.get('/viewEmpAppliedLeave', (request, response) => {
  const statement = `
  select  * from apply_leave

  `
  db.query(statement, (error, datas) => {
    if (error) {
      response.send({status: 'error', error: error})
    } else {
      if (datas.length == 0) {
        response.send({status: 'error', error: 'data does not exist'})
      } else {
       
        response.send(utils.createResult(error, datas))
      }
    }
  })
})

// ----------------------------------------------------


//viewEmpQueries()
router.get('/viewEmpQueries', (request, response) => {
  const statement = `
  select  * from  emp_queries

  `
  db.query(statement, (error, datas) => {
    if (error) {
      response.send({status: 'error', error: error})
    } else {
      if (datas.length == 0) {
        response.send({status: 'error', error: 'data does not exist'})
      } else {
       
        response.send(utils.createResult(error, datas))
      }
    }
  })
})


// ----------------------------------------------------

//searchEmpAndFindHisAttendanceOfMonth
router.get('/EmpAndHisAttendanceOfMonth', (request, response) => {
  const statement = `
  select e.empId, e.empName, 30 - count(a.leaveStatus = "approved") as totalDayPresent from
   employee e inner join apply_leave a on e.empId = a.empId where a.leavestatus = "approved" 
  group by e.empId,e.empName,a.leaveStatus ;

  `
  db.query(statement, (error, datas) => {
    if (error) {
      response.send({status: 'error', error: error})
    } else {
      if (datas.length == 0) {
        response.send({status: 'error', error: 'data does not exist'})
      } else {
       
        response.send(utils.createResult(error, datas))
      }
    }
  })
})



// ----------------------------------------------------

//searchEmpAndViewSalarySlip
router.get('/EmpAndHisAttendanceOfMonth/:empId', (request, response) => {
  
  const {empId} = request.params
  
  const statement = `
  select e.empId, s.salId,s.totalAmount from  employee e inner join salary_slip s on e.empId = s.empId
 where e.empId =${empId}
  `
  db.query(statement, (error, datas) => {
    if (error) {
      response.send({status: 'error', error: error})
    } else {
      if (datas.length == 0) {
        response.send({status: 'error', error: 'data does not exist'})
      } else {
       
        response.send(utils.createResult(error, datas))
      }
    }
  })
})


// ----------------------------------------------------

// ----------------------------------------------------
// POST
// ----------------------------------------------------

//addNewEmployee

router.post('/addNewEmployee', (request, response) => {
  const {  empName, address, birth_date, gendor, email,mobile, password , role } = request.body

  const encryptedPassword = crypto.SHA256(password)
  const statement = `insert into employee ( empName, address, birth_date, gendor, email, mobile, password , role ) values (
     '${empName}', '${address}', '${birth_date}' , '${gendor}', '${email}' , '${mobile}', '${encryptedPassword}', '${role}'
  )`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------

//addNewMeetingDetails

router.post('/addNewMeetingDetails', (request, response) => {
  const {  meetingDate,  meetingInfo,   meetingStatus} = request.body

  
  const statement = `insert into meeting (  meetingDate,  meetingInfo,   meetingStatus) values (
     '${meetingDate}', '${meetingInfo}', '${meetingStatus}' 
  )`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------


router.post('/signin', (request, response) => {
  const {Email, Password} = request.body
  const statement = `select id, FirstName, LastName from Admin where Email = '${Email}' and Password = '${crypto.SHA256(Password)}'`
  db.query(statement, (error, admins) => {
    if (error) {
      response.send({status: 'error', error: error})
    } else {
      if (admins.length == 0) {
        response.send({status: 'error', error: 'Admin does not exist'})
      } else {
        const admin = admins[0]
       
        response.send(utils.createResult(error, {
          FirstName: admin['FirstName'],
          LastName: admin['LastName']
          
        }))
      }
    }
  })
})


// ----------------------------------------------------

//generateSalarySlipForEmp

router.post('/generateSalarySlipForEmp', (request, response) => {
 
  
  const {  noOfPresentDay, leaveCount, totalAmount ,  adminId, empId } = request.body

  
  const statement = `insert into salary_slip (noOfPresentDay, leaveCount, totalAmount, adminId, empId ) values (
    ${noOfPresentDay}', '${leaveCount}', '${((noOfPresentDay - leaveCount ) * (800))}' , '${adminId}', '${empId}' 
  )`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})



// ----------------------------------------------------


// ----------------------------------------------------
// PUT
// ----------------------------------------------------
//ApproveEmpAppliedLeave
router.put('/ApproveEmpAppliedLeave/:leaveId', (request, response) => {
  const {leaveId} = request.params
  const {leaveStatus} = request.body
  const statement = `update apply_leave set leaveStatus = '${leaveStatus}' where leaveId = ${leaveId}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})



// ----------------------------------------------------

module.exports = router