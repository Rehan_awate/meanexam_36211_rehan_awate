
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {  ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class AdminService implements CanActivate {
  url = 'http://localhost:3000/admin'

  constructor(
    private router: Router,
    private httpClient : HttpClient
  ) { }


  login(Email: string, Password: string) {
    const body = {
      Email:  Email,
      Password: Password
    }

    return this.httpClient.post(this.url + '/signin', body)
  }

  
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (sessionStorage['FirstName']) {
      // user is already logged in
      // launch the component
      return true
    }

    // force user to login
    this.router.navigate(['/signin'])

    // user has not logged in yet
    // stop launching the component
    return false 
  }
}


