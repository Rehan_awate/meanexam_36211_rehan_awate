import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import{ MeetingService } from './../meeting.service'

@Component({
  selector: 'app-meeting',
  templateUrl: './meeting.component.html',
  styleUrls: ['./meeting.component.css']
})
export class MeetingComponent implements OnInit {

  meetings = []

  meeting = null
  meetingDate =''
  meetingInfo=''
  meetingStatus =''
  adminId = 0

  constructor(
    private meetingService:MeetingService,
    private router:Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(
    
  ): void {

    const id = this.activatedRoute.snapshot
    if (id) {
      // edit product
      this.meetingService
        .getmeetingListAndMeetingStatus()
        .subscribe(response => {
          if (response['status'] == 'success') {
            const meetings = response['data']
            if (meetings.length > 0) {
              this.meeting = meetings[0]
               this.meetingDate = this.meeting['meetingDate']
              this.meetingDate = this.meeting['meetingDate']
              this.meetingInfo = this.meeting['meetingInfo']
              this.adminId = this.meeting['adminId']
              
            }
          }
        })
    }
    this.loadMeetings()
  }

  loadMeetings() {
    this.meetingService
      .getmeetingListAndMeetingStatus()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.meetings = response['data']
          console.log(response['data'])
        } else {
          console.log(response['error'])
        }
      })
  }
  


}
