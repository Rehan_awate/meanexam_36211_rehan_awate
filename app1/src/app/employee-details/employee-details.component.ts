import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import {EmployeeService} from './../employee.service'
@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {

  employees = []

  employee = null

  empName=''
  totalDayPresent =''


  constructor(
    private employeeDetailsComponent:EmployeeService,
    private router:Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(
    
  ): void {

    const id = this.activatedRoute.snapshot
    if (id) {
      // edit product
      this.employeeDetailsComponent
        .getEmpAndHisAttendanceOfMonth()
        .subscribe(response => {
          if (response['status'] == 'success') {
            const Students = response['data']
            if (Students.length > 0) {
              this.employee = this.employees[0]
               this.empName = this.employee['empName']
              this.totalDayPresent = this.employee['totalDayPresent']
              
              
            }
          }
        })
    }
    this.getEmpAndHisAttendanceOfMonth()
  }

  getEmpAndHisAttendanceOfMonth() {
    this.employeeDetailsComponent
      .getEmpAndHisAttendanceOfMonth()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.employees = response['data']
          console.log(response['data'])
        } else {
          console.log(response['error'])
        }
      })
  }
  
}
