import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({

  providedIn: 'root'
})
export class MeetingService {

  url = 'http://localhost:3000/admin'

  constructor(
    private httpClient : HttpClient) { }

  
  getmeetingListAndMeetingStatus() {
   
    
    return this.httpClient.get(this.url + '/meetingListAndMeetingStatus' )
 
  }

  // getStudentsByClass() {
   
    
  //   return this.httpClient.get(this.url + '/GetStudentByRoom' )
 
  // }
}
