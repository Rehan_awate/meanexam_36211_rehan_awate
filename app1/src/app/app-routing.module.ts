import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SigninComponent} from './signin/signin.component'
import {MeetingComponent} from './meeting/meeting.component'
import {AdminService} from './admin.service'
//import {EmployeeDetailsComponent} from './employee-details/employee-details.component'
const routes: Routes = [
  { path: 'signin', component: SigninComponent },
  { path: 'meeting', component: MeetingComponent, canActivate : [AdminService] },
  { path: 'employee-details', component:EmployeeDetailsComponent, canActivate : [AdminService] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
