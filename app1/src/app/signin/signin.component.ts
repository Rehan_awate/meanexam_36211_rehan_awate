import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {AdminService} from './../admin.service'

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  Email = ''
  Password = ''

  

  constructor(
    private router: Router,
    private adminService: AdminService) {}

  ngOnInit(): void {
  }

  onLogin() {
    this.adminService
      .login(this.Email, this.Password)
      .subscribe(response => {
        if (response['status'] == 'success') {
          const data = response['data']
          console.log(data)

          // // cache the user info
          
          sessionStorage['FirstName'] = data['FirstName']
          sessionStorage['LastName'] = data['LastName']

          // goto the dashboard
          this.router.navigate(['/meeting'])

        } else {
          alert('invalid email or password')
        }
      })
  }

}